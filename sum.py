def sum_2(x, y):
    total = x + y
    if total in range(15, 20):
        return 20
    else:
        return total

print(sum_2(10, 6))
print(sum_2(10, 2))
print(sum_2(10, 12))